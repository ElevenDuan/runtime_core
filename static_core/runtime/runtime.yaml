# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Mapping beetween managed core types and C++ mirror classes
coretypes:
- managed_class: panda.Object
  mirror_class: ObjectHeader

- managed_class: panda.String
  mirror_class: coretypes::String

- managed_class: panda.Class
  mirror_class: coretypes::Class

# Namespace that contains intrinsics implementation. For functions from
# this namespace declaration in intrinsics.h will be generated
intrinsics_namespace: ark::intrinsics

# NB! The `space` property allows to group intrinsics and control
# which intrinsics are loaded during startup. By default only "core"
# space is loaded. See --load-runtimes for more details.
#
# The `signature` for instance intrinsics (`static` is false) doesn't include
# `this` argument.
intrinsics:
- name: MathAbsI32
  space: core
  class_name: Math
  method_name: absI32
  static: true
  signature:
    ret: i32
    args:
      - i32
  impl: ark::intrinsics::AbsI32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathAbsI64
  space: core
  class_name: Math
  method_name: absI64
  static: true
  signature:
    ret: i64
    args:
      - i64
  impl: ark::intrinsics::AbsI64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathAbsF32
  space: core
  class_name: Math
  method_name: absF32
  static: true
  signature:
    ret: f32
    args:
      - f32
  impl: ark::intrinsics::AbsF32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathAbsF64
  space: core
  class_name: Math
  method_name: absF64
  static: true
  signature:
    ret: f64
    args:
      - f64
  impl: ark::intrinsics::AbsF64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMinI32
  space: core
  class_name: Math
  method_name: minI32
  static: true
  signature:
    ret: i32
    args: [i32, i32]
  impl: ark::intrinsics::MinI32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMinI64
  space: core
  class_name: Math
  method_name: minI64
  static: true
  signature:
    ret: i64
    args: [i64, i64]
  impl: ark::intrinsics::MinI64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMinF32
  space: core
  class_name: Math
  method_name: minF32
  static: true
  signature:
    ret: f32
    args: [f32, f32]
  impl: ark::intrinsics::MinF32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMinF64
  space: core
  class_name: Math
  method_name: minF64
  static: true
  signature:
    ret: f64
    args: [f64, f64]
  impl: ark::intrinsics::MinF64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMaxI32
  space: core
  class_name: Math
  method_name: maxI32
  static: true
  signature:
    ret: i32
    args: [i32, i32]
  impl: ark::intrinsics::MaxI32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMaxI64
  space: core
  class_name: Math
  method_name: maxI64
  static: true
  signature:
    ret: i64
    args: [i64, i64]
  impl: ark::intrinsics::MaxI64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMaxF32
  space: core
  class_name: Math
  method_name: maxF32
  static: true
  signature:
    ret: f32
    args: [f32, f32]
  impl: ark::intrinsics::MaxF32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathMaxF64
  space: core
  class_name: Math
  method_name: maxF64
  static: true
  signature:
    ret: f64
    args: [f64, f64]
  impl: ark::intrinsics::MaxF64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: DoubleIsInf
  space: core
  class_name: Double
  method_name: isInfinite
  static: true
  signature:
    ret: u1
    args:
      - f64
  impl: ark::intrinsics::IsInfF64
  codegen_func: CreateFloatIsInf
  llvm_codegen_func: EmitIsInf
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: FloatIsInf
  space: core
  class_name: Float
  method_name: isInfinite
  static: true
  signature:
    ret: u1
    args:
      - f32
  impl: ark::intrinsics::IsInfF32
  codegen_func: CreateFloatIsInf
  llvm_codegen_func: EmitIsInf
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathSinF32
  space: core
  class_name: Math
  method_name: fsin
  static: true
  signature:
    ret: f32
    args:
      - f32
  impl: ark::intrinsics::SinF32
  llvm_impl: sin
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathSinF64
  space: core
  class_name: Math
  method_name: sin
  static: true
  signature:
    ret: f64
    args:
      - f64
  impl: ark::intrinsics::SinF64
  llvm_impl: sin
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathCosF32
  space: core
  class_name: Math
  method_name: fcos
  static: true
  signature:
    ret: f32
    args:
      - f32
  impl: ark::intrinsics::CosF32
  llvm_impl: cos
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathCosF64
  space: core
  class_name: Math
  method_name: cos
  static: true
  signature:
    ret: f64
    args:
      - f64
  impl: ark::intrinsics::CosF64
  llvm_impl: cos
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathPowF32
  space: core
  class_name: Math
  method_name: fpow
  static: true
  signature:
    ret: f32
    args:
      - f32
      - f32
  impl: ark::intrinsics::PowF32
  llvm_impl: pow
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathPowF64
  space: core
  class_name: Math
  method_name: pow
  static: true
  signature:
    ret: f64
    args:
      - f64
      - f64
  impl: ark::intrinsics::PowF64
  llvm_impl: pow
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathSqrtF32
  space: core
  class_name: Math
  method_name: fsqrt
  static: true
  signature:
    ret: f32
    args:
      - f32
  impl: ark::intrinsics::SqrtF32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathSqrtF64
  space: core
  class_name: Math
  method_name: sqrt
  static: true
  signature:
    ret: f64
    args:
      - f64
  impl: ark::intrinsics::SqrtF64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: MathCalculateDouble
  space: core
  class_name: Math
  method_name: calculateDouble
  static: true
  private: true
  signature:
    ret: f64
    args: [u32, f64]
  impl: ark::intrinsics::CalculateDouble

- name: MathCalculateFloat
  space: core
  class_name: Math
  method_name: calculateFloat
  static: true
  private: true
  signature:
    ret: f32
    args: [u32, f32]
  impl: ark::intrinsics::CalculateFloat

- name: IOPrintString
  space: core
  class_name: IO
  method_name: printString
  static: true
  signature:
    ret: void
    args:
      - panda.String
  impl: ark::intrinsics::PrintString
  clear_flags: [require_state, runtime_call]

- name: IOPrintF32
  space: core
  class_name: IO
  method_name: printF32
  static: true
  signature:
    ret: void
    args:
      - f32
  impl: ark::intrinsics::PrintF32
  clear_flags: [require_state, runtime_call]

- name: IOPrintF64
  space: core
  class_name: IO
  method_name: printF64
  static: true
  signature:
    ret: void
    args:
      - f64
  impl: ark::intrinsics::PrintF64
  clear_flags: [require_state, runtime_call]

- name: IOPrintI32
  space: core
  class_name: IO
  method_name: printI32
  static: true
  signature:
    ret: void
    args:
      - i32
  impl: ark::intrinsics::PrintI32
  clear_flags: [require_state, runtime_call]

- name: IOPrintU32
  space: core
  class_name: IO
  method_name: printU32
  static: true
  signature:
    ret: void
    args:
      - u32
  impl: ark::intrinsics::PrintU32
  clear_flags: [require_state, runtime_call]

- name: IOPrintI64
  space: core
  class_name: IO
  method_name: printI64
  static: true
  signature:
    ret: void
    args:
      - i64
  impl: ark::intrinsics::PrintI64
  clear_flags: [require_state, runtime_call]

- name: IOPrintU64
  space: core
  class_name: IO
  method_name: printU64
  static: true
  signature:
    ret: void
    args:
      - u64
  impl: ark::intrinsics::PrintU64
  clear_flags: [require_state, runtime_call]

- name: SystemCompileMethod
  space: core
  class_name: System
  method_name: compileMethod
  static: true
  private: true
  signature:
    ret: u8
    args: [panda.String]
  impl: ark::intrinsics::CompileMethod

- name: SystemExit
  space: core
  class_name: System
  method_name: exit
  static: true
  signature:
    ret: void
    args:
      - i32
  impl: ark::intrinsics::SystemExit
  clear_flags: [require_state, runtime_call]

- name: SystemNanoTime
  space: core
  class_name: System
  method_name: nanoTime
  static: true
  signature:
    ret: i64
    args: []
  impl: ark::intrinsics::NanoTime
  clear_flags: [require_state, runtime_call]

- name: SystemAssert
  space: core
  class_name: System
  method_name: assert
  static: true
  signature:
    ret: void
    args: [u1]
  impl: ark::intrinsics::Assert
  clear_flags: [require_state, runtime_call]

- name: SystemAssertPrint
  space: core
  class_name: System
  method_name: assertPrint
  static: true
  signature:
    ret: void
    args: [u1, panda.String]
  impl: ark::intrinsics::AssertPrint
  clear_flags: [require_state, runtime_call]

- name: SystemScheduleCoroutine
  space: core
  class_name: System
  method_name: scheduleCoroutine
  static: true
  signature:
    ret: void
    args: []
  impl: ark::intrinsics::SystemScheduleCoroutine

- name: SystemCoroutineGetWorkerId
  space: core
  class_name: System
  method_name: getCoroutineWorkerId
  static: true
  signature:
    ret: i32
    args: []
  impl: ark::intrinsics::SystemCoroutineGetWorkerId

- name: CheckTag
  space: core
  class_name: DebugUtils
  method_name: checkTag
  static: true
  signature:
    ret: void
    args: [i64, i64]
  impl: ark::intrinsics::CheckTag
  clear_flags: [require_state, runtime_call]

- name: ConvertStringToI32
  space: core
  class_name: Convert
  method_name: stringToI32
  static: true
  signature:
    ret: i32
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToI32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ConvertStringToU32
  space: core
  class_name: Convert
  method_name: stringToU32
  static: true
  signature:
    ret: u32
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToU32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ConvertStringToI64
  space: core
  class_name: Convert
  method_name: stringToI64
  static: true
  signature:
    ret: i64
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToI64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ConvertStringToU64
  space: core
  class_name: Convert
  method_name: stringToU64
  static: true
  signature:
    ret: u64
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToU64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ConvertStringToF32
  space: core
  class_name: Convert
  method_name: stringToF32
  static: true
  signature:
    ret: f32
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToF32
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ConvertStringToF64
  space: core
  class_name: Convert
  method_name: stringToF64
  static: true
  signature:
    ret: f64
    args: [panda.String]
  impl: ark::intrinsics::ConvertStringToF64
  clear_flags: [no_dce, no_hoist, no_cse, barrier, require_state, runtime_call]

- name: ObjectCreateNonMovable
  space: core
  class_name: Object
  method_name: createNonMovable
  static: true
  signature:
    ret: panda.Object
    args: [panda.Class]
  impl: ark::intrinsics::ObjectCreateNonMovable
  set_flags: [can_throw]

- name: ObjectMonitorEnter
  space: core
  class_name: Object
  method_name: monitorEnter
  static: true
  signature:
    ret: void
    args: [panda.Object]
  impl: ark::intrinsics::ObjectMonitorEnter

- name: ObjectMonitorExit
  space: core
  class_name: Object
  method_name: monitorExit
  static: true
  signature:
    ret: void
    args: [panda.Object]
  impl: ark::intrinsics::ObjectMonitorExit

- name: ObjectWait
  space: core
  class_name: Object
  method_name: Wait
  static: true
  signature:
    ret: void
    args: [panda.Object]
  impl: ark::intrinsics::ObjectWait
  set_flags: [heap_inv]

- name: ObjectTimedWait
  space: core
  class_name: Object
  method_name: TimedWait
  static: true
  signature:
    ret: void
    args: [panda.Object, u64]
  impl: ark::intrinsics::ObjectTimedWait
  set_flags: [heap_inv]

- name: ObjectTimedWaitNanos
  space: core
  class_name: Object
  method_name: TimedWaitNanos
  static: true
  signature:
    ret: void
    args: [panda.Object, u64, u64]
  impl: ark::intrinsics::ObjectTimedWaitNanos
  set_flags: [heap_inv]

- name: ObjectNotify
  space: core
  class_name: Object
  method_name: Notify
  static: true
  signature:
    ret: void
    args: [panda.Object]
  impl: ark::intrinsics::ObjectNotify
  clear_flags: [require_state, runtime_call]

- name: ObjectNotifyAll
  space: core
  class_name: Object
  method_name: NotifyAll
  static: true
  signature:
    ret: void
    args: [panda.Object]
  impl: ark::intrinsics::ObjectNotifyAll
  clear_flags: [require_state, runtime_call]
